﻿using UnityEngine;

namespace Common.Views
{
    public interface IHideHandler
    {
        void Init(GameObject gameObject);
        void Hide();
    }
}