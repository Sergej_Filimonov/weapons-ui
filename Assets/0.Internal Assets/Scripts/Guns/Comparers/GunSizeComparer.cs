﻿namespace Guns
{
    public class GunSizeComparer : GunComparer<GunItem>
    {
        public override int Compare(GunItem x, GunItem y)
        {
            int result = 0;
            if (x.Size < y.Size)
            {
                result = -1;
            }
            else if (x.Size > y.Size)
            {
                result = 1;
            }
            return result;
        }
    }
}