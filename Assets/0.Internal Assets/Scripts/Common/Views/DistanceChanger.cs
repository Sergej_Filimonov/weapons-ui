﻿using DG.Tweening;
using UnityEngine;

namespace Common.Views
{
    public class DistanceChanger : MonoBehaviour, IShowHandler, IHideHandler
    {
        [SerializeField] private float hideZ = 100;
        [SerializeField] private float time = 1;

        private Transform objTransform;

        public void Init(GameObject gameObject)
        {
            objTransform = gameObject.transform;
        }

        public void Show()
        {
            objTransform.DOKill();
            objTransform.DOLocalMoveZ(0, time);
        }

        public void Hide()
        {
            objTransform.DOKill();
            objTransform.DOLocalMoveZ(hideZ, time);
        }
    }
}