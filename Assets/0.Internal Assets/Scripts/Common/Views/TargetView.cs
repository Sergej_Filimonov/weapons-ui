﻿using UnityEngine;

namespace Common.Views
{
	public abstract class TargetView<TTarget> : View
	{
		public TTarget Target { get; private set; }
        protected bool hasTarget { get; private set; } = false;

        public void Init(TTarget target)
        {
            Target = target;
            hasTarget = true;

            InitShowingAndHidingHandlers();
            Init();
            IsInited = true;
        }

        public new void Show()
		{
            if (!hasTarget)
            {
                Debug.LogError("No target! Viewer name:" + gameObject.name);
                return;
            }
			base.Show();
		}
	}
}