﻿using Items;

namespace Guns
{
    public class GunComparer<TGun> : ItemComparer<TGun> where TGun : GunItem
    {
        public override int Compare(TGun x, TGun y)
        {
            return x.CompareTo(y);
        }
    }
}