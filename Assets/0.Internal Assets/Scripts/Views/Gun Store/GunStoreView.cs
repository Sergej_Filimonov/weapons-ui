﻿using Common.Views;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Guns.Views
{
    public class GunStoreView : TargetView<IGunStore>
    {
        [SerializeField] protected Button home = null;
        [SerializeField] protected Button back = null;
        [SerializeField] protected GameObject itemPrefab = null;
        [SerializeField] protected Transform itemsContainer = null;

        public event Action<GunStoreView> Showed;
        public event Action<GunStoreView> Closed;

        public event Action<GunStoreView> HomeButtonClicked;
        public event Action<GunStoreView> BackButtonClicked;

        protected override void Init()
        {
            home.onClick.RemoveAllListeners();
            home.onClick.AddListener(() => HomeButtonClicked?.Invoke(this));

            back.onClick.RemoveAllListeners();
            back.onClick.AddListener(() => BackButtonClicked?.Invoke(this));
        }

        protected override void OnShown()
        {
            Showed?.Invoke(this);
        }

        protected override void BeforeHide()
        {
            Closed?.Invoke(this);
        }
    }
}