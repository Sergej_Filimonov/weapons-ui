﻿namespace Guns.Proxy
{
    public class GunProxyStats
    {
        public int size;
        public float range;
        public float reload_time;
        public GunProxyDamage damage;
    }
}