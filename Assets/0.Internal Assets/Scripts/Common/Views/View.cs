﻿using System;
using UnityEngine;

namespace Common.Views
{
	public abstract class View : MonoBehaviour, IDisposable
	{
		[SerializeField] protected GameObject objectToHide;
		[SerializeField] protected bool autoInit = false;

        public bool IsActive { get; protected set; } = false;
		public bool IsInited { get; protected set; } = false;

		protected IShowHandler showHandler;
		protected IHideHandler hideHandler;

		protected virtual void Awake()
		{
			if (objectToHide == null) objectToHide = gameObject;
            if (autoInit)
            {
				IsActive = objectToHide.activeSelf;
				if (IsActive)
				{
					ShowAndInit();
				}
			}
		}

		private void ShowAndInit()
        {
			InitShowingAndHidingHandlers();
			Init();
			IsInited = true;
			Show();
		}

		public void Show()
		{
			if (IsActive) return;
			if (!IsInited)
			{
				InitShowingAndHidingHandlers();
				Init();
				IsInited = true;
			}
			if (showHandler != null) showHandler.Show();
			OnShown();
			IsActive = true;
		}

		protected virtual void InitShowingAndHidingHandlers()
        {
			showHandler = new SimpleShowAndHideHandler();
			showHandler.Init(objectToHide);
			hideHandler = showHandler as IHideHandler;
        }

		protected virtual void Init() { }

		protected virtual void OnShown() {}

        protected virtual void BeforeHide() {}

        public virtual void Hide()
		{
			if (!IsActive) return;
            BeforeHide();
			if (hideHandler != null) hideHandler.Hide();
			IsActive = false;
		}

		public virtual void Dispose() { }

        protected virtual void OnDestroy() => Dispose();
    }
}