﻿using UnityEngine;

namespace Common.Views
{
    public class SimpleShowAndHideHandler : IShowHandler, IHideHandler
    {
        private GameObject gameObject;

        public void Init(GameObject gameObject)
        {
            this.gameObject = gameObject;
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}