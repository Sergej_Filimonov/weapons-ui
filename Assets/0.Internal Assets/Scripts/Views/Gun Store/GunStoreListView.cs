﻿using System;
using System.Collections.Generic;
using UI;
using UnityEngine;
using static UI.SortButtons;

namespace Guns.Views
{
    public class GunStoreListView : GunStoreView
    {
        [Space]
        [Header("Sort")]
        [SerializeField] private SortButtons sortButtons = null;

        public event Action<GunItemListView> ItemViewSelectClicked;

        private List<GunItemListView> views = new List<GunItemListView>();

        protected override void Init()
        {
            base.Init();

            foreach (var item in Target.Guns)
            {
                var view = Instantiate(itemPrefab, itemsContainer).GetComponent<GunItemListView>();
                view.Init(item);
                view.Show();
                view.SelectButtonClicked += v => ItemViewSelectClicked?.Invoke(v);
                views.Add(view);
            }

            sortButtons.SortingChanged += ChangeSort;
            ChangeSort(sortButtons.CurrentSort);
        }

        protected override void OnShown()
        {
            base.OnShown();

            ChangeSort(sortButtons.CurrentSort);
        }

        private void ChangeSort(SortType sortType)
        {
            switch (sortType)
            {
                case SortType.Name:
                    Target.Guns.SortByName();
                    break;
                case SortType.Size:
                    Target.Guns.SortBySize();
                    break;
                case SortType.Level:
                    Target.Guns.SortByLevel();
                    break;
                default:
                    Target.Guns.SortByID();
                    break;
            }

            foreach (var view in views)
            {
                int index = Target.Guns.IndexOf(view.Target);

                if (index == 0)
                {
                    view.transform.SetAsFirstSibling();
                }
                else if (index == Target.Guns.Count - 1)
                {
                    view.transform.SetAsLastSibling();
                }
                else
                {
                    view.transform.SetSiblingIndex(index);
                }
            }
        }
    }
}