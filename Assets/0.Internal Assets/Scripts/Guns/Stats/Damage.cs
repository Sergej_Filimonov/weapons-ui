﻿using System;

namespace Guns
{
    [Serializable]
    public struct Damage
    {
        public int Min;
        public int Max;
    }
}