﻿using Guns.Proxy;

namespace Networking
{
    public class Response
    {
        public string status;
        public GunsProxyCollection response;
    }
}