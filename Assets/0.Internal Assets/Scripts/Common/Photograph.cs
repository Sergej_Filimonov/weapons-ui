﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common.Queueable;
using static Common.Queueable.QueueableAction;

namespace Common
{
    public class Photograph : MonoBehaviour
    {
        [SerializeField] private Transform modelPosition = null;
        [SerializeField] private Camera cameraForRender = null;
        [SerializeField] private RenderTexture renderTexturePrefab = null;

        public static Photograph Instance;

        private event Action photoCreated;
        private QueueableActionsManager queueableActionsManager;
        private GameObject model;

        private void Awake()
        {
            Instance = this;

            queueableActionsManager = new QueueableActionsManager();
        }

        public void MakePhoto(GameObject model, Action<RenderTexture> callback)
        {
            var action = new QueueableAction(completeCallback =>
            {
                model = Instantiate(model, modelPosition);
                RenderTexture renderTexture = new RenderTexture(renderTexturePrefab);
                cameraForRender.targetTexture = renderTexture;
                cameraForRender.gameObject.SetActive(true);
                StartCoroutine(PassOneFrame(renderTexture, callback));
                photoCreated = null;
                photoCreated += () => GameObject.Destroy(model);
                photoCreated += completeCallback.Invoke;
            });

            queueableActionsManager.Add(action);
        }

        private IEnumerator PassOneFrame(RenderTexture renderTexture, Action<RenderTexture> callback)
        {
            yield return null;
            callback?.Invoke(renderTexture);
            cameraForRender.targetTexture = null;
            cameraForRender.gameObject.SetActive(false);
            yield return null;
            photoCreated?.Invoke();
        }
    }
}