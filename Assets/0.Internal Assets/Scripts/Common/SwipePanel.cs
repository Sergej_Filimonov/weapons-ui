﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Common
{
    public class SwipePanel : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
    {
        private const int Threshold = 10;

        public event Action<SwipeDirection> Swipe;

        private Vector3 startPosition;
        private Vector3 endPosition;
        private Vector3 delta;

        public void OnBeginDrag(PointerEventData eventData)
        {
            startPosition = eventData.position;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            endPosition = eventData.position;
            delta = startPosition - endPosition;

            if (Mathf.Abs(delta.x) > Threshold || Mathf.Abs(delta.y) > Threshold)
            {
                if (Mathf.Abs(delta.x) > Mathf.Abs(delta.y))
                {
                    if (delta.x < 0)
                    {
                        Swipe?.Invoke(SwipeDirection.Right);
                        Debug.Log("Swipe: Right");
                    }
                    else
                    {
                        Swipe?.Invoke(SwipeDirection.Left);
                        Debug.Log("Swipe: Left");
                    }
                }
                else
                {
                    if (delta.y < 0)
                    {
                        Swipe?.Invoke(SwipeDirection.Up);
                        Debug.Log("Swipe: Up");
                    }
                    else
                    {
                        Swipe?.Invoke(SwipeDirection.Down);
                        Debug.Log("Swipe: Down");
                    }
                }
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
        }
    }

    public enum SwipeDirection
    {
        Up,
        Down,
        Left,
        Right
    }
}