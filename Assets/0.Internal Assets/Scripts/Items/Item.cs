﻿using System;

namespace Items
{
    [Serializable]
    public class Item : IComparable<Item>
    {
        protected int id = 0;
        protected string itemName = "";
        protected string description = "";

        public int Id { get => id; }
        public string ItemName { get => itemName; }
        public string Description { get => description; }

        public Item(int id, string itemName, string description)
        {
            this.id = id;
            this.itemName = itemName;
            this.description = description;
        }

        public int CompareTo(Item other)
        {
            if (Id > other.Id)
            {
                return 1;
            }
            else if (Id == other.Id)
            {
                throw new Exception("Items has equals id!");
            }
            else
            {
                return -1;
            }
        }
    }
}