﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UI;
using Object = System.Object;

namespace Common.Helpers
{
	public static class AddressableHelper
	{
		private static readonly Dictionary<string, object> Cache = new Dictionary<string, object>();

		public static void LoadObject<T>(string addressableId, Action<T> callback) where T : class
		{
			if (!Cache.ContainsKey(addressableId))
			{
				Addressables.LoadAssetAsync<T>(addressableId).Completed +=
					result =>
					{
						if (!Cache.ContainsKey(addressableId)) Cache.Add(addressableId, result.Result);

						Cache[addressableId] = result.Result;
						callback?.Invoke(result.Result);
					};
			}
			else
			{
				callback?.Invoke((T)Cache[addressableId]);
			}
		}

		public static void LoadSprite(string addressableId, Image image)
		{
			LoadObject<Sprite>(addressableId, (sprite) => image.sprite = sprite);
		}

        public static void LoadCollection<T>(string label, Action<IEnumerable<T>> callback) where T : class
		{
			Addressables.LoadAssetsAsync<T>(label, null).Completed +=
				(result) => { callback?.Invoke(result.Result); };
		}

		public static void LoadAssetAsync<TObject>(this AssetReference source,
			Action<TObject> callback)
		{

			void LoadCompleted(AsyncOperationHandle<TObject> operationHandle) =>
				callback?.Invoke(operationHandle.Result);
			source.LoadAssetAsync<TObject>().Completed += LoadCompleted;
		}

		public static void LoadAssetCollection<T>(List<AssetReference> assetReferences, Action callback)
        {
			int loaded = 0;

			foreach (var reference in assetReferences)
			{
				reference.LoadAssetAsync<T>().Completed += a => { loaded++; if (loaded >= assetReferences.Count) callback?.Invoke(); };
			}
		}

		public static void LoadAssetCollection<T, K>(List<int> mapType, List<AssetReference> assetReferences, Action callback)
        {
            if (mapType.Count != assetReferences.Count) return;

            int loaded = 0;

            foreach (var reference in assetReferences)
            {
                int typeNumber = mapType[assetReferences.IndexOf(reference)];
                switch (typeNumber)
                {
                    case 0:
                        reference.LoadAssetAsync<T>().Completed += a => { loaded++; if (loaded >= assetReferences.Count) callback?.Invoke(); };
                        break;
                    case 1:
                        reference.LoadAssetAsync<K>().Completed += a => { loaded++; if (loaded >= assetReferences.Count) callback?.Invoke(); }; ;
                        break;
                    default:
                        Debug.LogError("LoadAssetCollection exeption!");
                        return;
                }
            }
        }
    }
}