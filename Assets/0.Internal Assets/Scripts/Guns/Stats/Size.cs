﻿using System;

namespace Guns
{
    [Serializable]
    public enum Size
    {
        S,
        M,
        L,
        XL
    }
}