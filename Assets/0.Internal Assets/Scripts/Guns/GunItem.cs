﻿using Items;
using System;

namespace Guns
{
    [Serializable]
    public class GunItem : Item
    {
        private int level = 1;
        private Size size = Size.S;
        private float range = 1;
        private float reloadTime = 1;
        private Damage damage;

        public int Level { get => level; }
        public Size Size { get => size; }
        public float Range { get => range; }
        public float ReloadTime { get => reloadTime; }
        public Damage Damage { get => damage; }

        public GunItem(int id, string itemName, string description,
            int level, Size size, float range, float reloadTime, Damage damage) : base(id, itemName, description)
        {
            this.level = level;
            this.size = size;
            this.range = range;
            this.reloadTime = reloadTime;
            this.damage = damage;
        }
    }
}