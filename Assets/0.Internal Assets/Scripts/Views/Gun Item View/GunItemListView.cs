﻿using Common.Views;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Guns.Views
{
    public class GunItemListView : TargetView<GunItem>
    {
        [SerializeField] private TMP_Text itemName = null;
        [SerializeField] private TMP_Text itemSize = null;
        [SerializeField] private TMP_Text itemLevel = null;
        [SerializeField] private Button selectButton = null;

        public event Action<GunItemListView> SelectButtonClicked;

        protected override void Init()
        {
            selectButton.onClick.RemoveAllListeners();
            selectButton.onClick.AddListener(() => SelectButtonClicked?.Invoke(this));

            itemName.text = Target.ItemName;
            itemSize.text = Target.Size.ToString();
            itemLevel.text = string.Format("LVL {0}", Target.Level);
        }
    }
}