﻿using System;

namespace Common.Queueable
{
    public class QueueableAction
    {
        public delegate void CallbackHandler();

        public event Action Completed;
        public bool isRunning;

        private event Action<CallbackHandler> action;
        private CallbackHandler callback;

        public QueueableAction(Action<CallbackHandler> action)
        {
            isRunning = false;
            this.action = action;
            callback = () => Complete();
        }

        public void Run()
        {
            isRunning = true;
            action.Invoke(callback);
        }

        public void Complete()
        {
            isRunning = false;
            Completed?.Invoke();
            Completed = null;
        }
    }
}