﻿namespace Guns
{
    public interface IGunStore
    {
        bool IsInitialized { get; }
        GunCatalog Guns { get; }

        void OnInitialized(GunCatalog catalog);

        void OnInitializeFailed();
    }
}