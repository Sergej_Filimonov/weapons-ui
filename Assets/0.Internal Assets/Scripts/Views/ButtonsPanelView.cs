﻿using Common.Views;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class ButtonsPanelView : View
    {
        [SerializeField] private Button openList = null;
        [SerializeField] private Button openGrid = null;
        [SerializeField] private Button openSingle = null;

        public event Action<ButtonsPanelView> OpenListButtonClicked;
        public event Action<ButtonsPanelView> OpenGridButtonClicked;
        public event Action<ButtonsPanelView> OpenSingleButtonClicked;

        protected override void Init()
        {
            openList.onClick.RemoveAllListeners();
            openList.onClick.AddListener(() => OpenListButtonClicked?.Invoke(this));

            openGrid.onClick.RemoveAllListeners();
            openGrid.onClick.AddListener(() => OpenGridButtonClicked?.Invoke(this));

            openSingle.onClick.RemoveAllListeners();
            openSingle.onClick.AddListener(() => OpenSingleButtonClicked?.Invoke(this));
        }
    }
}