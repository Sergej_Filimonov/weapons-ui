﻿namespace Guns.Proxy
{
    public class GunProxyModel
    {
        public int id;
        public int level;
        public string name;
        public GunProxyStats stats;
    }
}