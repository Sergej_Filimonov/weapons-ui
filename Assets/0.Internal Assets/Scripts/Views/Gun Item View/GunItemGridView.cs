﻿using Common.Views;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Common;

namespace Guns.Views
{
    public class GunItemGridView : TargetView<GunItem>
    {
        [SerializeField] private TMP_Text itemName = null;
        [SerializeField] private TMP_Text itemSize = null;
        [SerializeField] private TMP_Text itemDescription = null;
        [SerializeField] private TMP_Text itemLevel = null;
        [SerializeField] private Button selectButton = null;
        [SerializeField] private RawImage gunImage = null;

        public event Action<GunItemGridView> SelectButtonClicked;

        public GunItemAsset GunAsset { get; private set; }

        protected override void Init()
        {
            selectButton.onClick.RemoveAllListeners();
            selectButton.onClick.AddListener(() => SelectButtonClicked?.Invoke(this));

            itemName.text = Target.ItemName;
            itemSize.text = Target.Size.ToString();
            itemLevel.text = string.Format("LVL {0}", Target.Level);

            GunAsset = ApplicationManager.Instance.GunAssetsList.GetAsset(Target.Id);

            itemDescription.text = GunAsset.Description;
            Photograph.Instance.MakePhoto((GameObject)GunAsset.Model.Asset, rt => gunImage.texture = rt);
        }
    }
}