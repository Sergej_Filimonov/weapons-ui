﻿using Items;

namespace Guns
{
    public class GunCatalog : ItemCatalog<GunItem>
    {
        public void SortByID()
        {
            items.Sort();
        }

        public void SortByName()
        {
            items.Sort(new ItemNameComparer());
        }

        public void SortByLevel()
        {
            items.Sort(new GunLevelComparer());
        }

        public void SortBySize()
        {
            items.Sort(new GunSizeComparer());
        }
    }
}