﻿using Common.Views;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class SortButtons : View
    {
        [SerializeField] private SortButton[] sortButtons = null;

        public event Action<SortType> SortingChanged;

        public SortType CurrentSort => currentSort;

        private SortType currentSort = SortType.Name;
        private Dictionary<SortType, SortButton> sortButtonsData = new Dictionary<SortType, SortButton>(3);

        protected override void Init()
        {
            foreach (var button in sortButtons)
            {
                button.Init();
                button.Clicked += ChangeSorting;
                sortButtonsData.Add(button.SortType, button);
            }

            ChangeSorting(SortType.Size);
        }

        private void ChangeSorting(SortType sortType)
        {
            if (sortType != currentSort)
            {
                sortButtonsData[currentSort].SetSelected(false);
                sortButtonsData[sortType].SetSelected(true);
                currentSort = sortType;
                SortingChanged?.Invoke(sortType);
                Debug.Log("Sorting changed! Current sorting = " + currentSort.ToString());
            }
        }

        [Serializable]
        private class SortButton
        {
            [SerializeField] private SortType sortType = SortType.Name;
            [SerializeField] private Button button = null;
            [SerializeField] private GameObject normalState = null;
            [SerializeField] private GameObject selectedState = null;

            public event Action<SortType> Clicked;

            public SortType SortType { get => sortType; }

            public void Init()
            {
                button.onClick.RemoveAllListeners();
                button.onClick.AddListener(() => Clicked?.Invoke(sortType));
            }

            public void SetSelected(bool value)
            {
                selectedState.SetActive(value);
                normalState.SetActive(!value);
            }
        }

        [Serializable]
        public enum SortType
        {
            Name,
            Size,
            Level
        }
    }
}