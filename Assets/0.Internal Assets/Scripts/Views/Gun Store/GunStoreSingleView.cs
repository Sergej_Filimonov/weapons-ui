﻿using Common;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using DG.Tweening;
using System.Linq;

namespace Guns.Views
{
    public class GunStoreSingleView : GunStoreView
    {
        [SerializeField] private SwipePanel swipePanel = null;
        [SerializeField] private CollectionScroll collectionScroll = null;
        [SerializeField] private CircleIndexIndicator indicator = null;
        [Space]
        [Header("Gun property")]
        [SerializeField] private float animationTime = 0.5f;
        [SerializeField] private CanvasGroup headerCanvasGroup = null;
        [SerializeField] private TMP_Text itemName = null;
        [SerializeField] private TMP_Text itemSize = null;
        [SerializeField] private GameObject[] sizeCells = null;
        [SerializeField] private TMP_Text itemLevel = null;
        [SerializeField] private CanvasGroup descriptionCanvasGroup = null;
        [SerializeField] private TMP_Text itemDescription = null;

        private List<GunItemSingleView> views = new List<GunItemSingleView>();

        protected override void Init()
        {
            base.Init();

            itemsContainer = collectionScroll.ObjectsContainer;

            swipePanel.Swipe += OnSwipe;

            foreach (var item in Target.Guns)
            {
                var view = Instantiate(itemPrefab, itemsContainer).GetComponent<GunItemSingleView>();
                view.Init(item);
                views.Add(view);
            }

            collectionScroll.SetCollection(views.Select(v => v.transform).ToList());
            indicator.Init(views.Count);

            views[0].Show();
            DrawGunItemProperty(views[0].Target, views[0].GunAsset.Description, true);
        }

        public void Show(GunItem gunItem)
        {
            Show();

            var view = views.FirstOrDefault(v => v.Target == gunItem);

            if (views.IndexOf(view) != collectionScroll.Pointer)
            {
                views[collectionScroll.Pointer].Hide();
                view.Show();
                DrawGunItemProperty(view.Target, view.GunAsset.Description, true);
                collectionScroll.SetAtCenter(view.transform);
                indicator.SetCurrentIndex(collectionScroll.Pointer);
            }
        }

        private void DrawGunItemProperty(GunItem item, string description = "", bool setVisible = false)
        {
            itemName.text = item.ItemName;
            itemSize.text = item.Size.ToString();
            itemLevel.text = "LVL " + item.Level;
            itemDescription.text = description;

            int size = (int)item.Size;
            for (int i = 0; i < sizeCells.Length; i++)
            {
                if (i <= size)
                {
                    sizeCells[i].SetActive(true);
                }
                else
                {
                    sizeCells[i].SetActive(false);
                }
            }

            if (setVisible)
            {
                headerCanvasGroup.alpha = 1;
                descriptionCanvasGroup.alpha = 1;
            }
        }

        private void DrawGunItemPropertyWithAnim(GunItem item, string description = "")
        {
            descriptionCanvasGroup.DOKill();
            descriptionCanvasGroup.DOFade(0, animationTime);
            headerCanvasGroup.DOKill();
            headerCanvasGroup.DOFade(0, animationTime).OnComplete(()=> 
            {
                DrawGunItemProperty(item, description);
                headerCanvasGroup.DOFade(1, animationTime);
                descriptionCanvasGroup.DOFade(1, animationTime);
            });
        }

        private void OnSwipe(SwipeDirection swipeDirection)
        {
            bool needAction = false;
            bool toLeft = false;

            switch (swipeDirection)
            {
                case SwipeDirection.Up:
                    break;
                case SwipeDirection.Down:
                    break;
                case SwipeDirection.Left:
                    needAction = true;
                    toLeft = true;
                    break;
                case SwipeDirection.Right:
                    needAction = true;
                    break;
                default:
                    break;
            }

            if (needAction)
            {
                views[collectionScroll.Pointer].Hide();
                collectionScroll.MoveObjects(toLeft);
                indicator.SetCurrentIndex(collectionScroll.Pointer, true);

                var view = views[collectionScroll.Pointer];
                view.Show();
                DrawGunItemPropertyWithAnim(view.Target, view.GunAsset.Description);
            }
        }
    }
}