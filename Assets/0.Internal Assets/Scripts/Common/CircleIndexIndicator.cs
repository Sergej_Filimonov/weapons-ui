﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Common
{
    public class CircleIndexIndicator : MonoBehaviour
    {
        [SerializeField] private Image firstCircle = null;
        [SerializeField] private AnimationCurve alpha = null;
        [SerializeField] private Color color = Color.white;
        [SerializeField] private Color selecColor = Color.white;
        [SerializeField] private AnimationCurve size = null;
        [SerializeField] private float animationTime = 1.0f;

        private RectTransform circlesParent;
        private Vector2 circleParentPosition = Vector2.zero;
        private Vector3 elementSize = Vector3.one;
        private int indexDelta = 0;
        private List<Image> circles;
        private float elementWidth;

        public void Init(int elementsCount)
        {
            circles = new List<Image>(elementsCount);
            circles.Add(firstCircle);
            circlesParent = firstCircle.transform.parent.GetComponent<RectTransform>();
            elementWidth = firstCircle.GetComponent<RectTransform>().rect.width;

            for (int i = 1; i < elementsCount; i++)
            {
                var newCircle = Instantiate(firstCircle, circlesParent);
                circles.Add(newCircle);
            }

            SetCurrentIndex(0);
        }

        public void SetCurrentIndex(int index, bool useAnimation = false)
        {
            for (int i = 0; i < circles.Count; i++)
            {
                indexDelta = Mathf.Abs(index - i);

                if (indexDelta == 0)
                {
                    if (useAnimation) circles[i].DOColor(selecColor, animationTime);
                    else circles[i].color = selecColor;
                }
                else
                {
                    color.a = 1 * alpha.Evaluate(indexDelta);
                    if (useAnimation) circles[i].DOColor(color, animationTime);
                    else circles[i].color = color;
                }

                elementSize = Vector3.one * size.Evaluate(indexDelta);
                if (useAnimation) circles[i].transform.DOScale(elementSize, animationTime);
                else circles[i].transform.localScale = elementSize;
            }

            circleParentPosition.x = -index * elementWidth;
            if (useAnimation) circlesParent.DOAnchorPos(circleParentPosition, animationTime);
            else circlesParent.anchoredPosition = circleParentPosition;
        }
    }
}