﻿using Guns;
using Networking;
using UnityEngine;
using UnityEngine.AddressableAssets;
using System.Collections;
using System.Collections.Generic;
using Common.Helpers;
using UI;

public class ApplicationManager : MonoBehaviour
{
    [SerializeField] private GunAssetsList gunAssetsList = null;
    [SerializeField] private UIController uiController = null;

    public static ApplicationManager Instance;

    public IGunStore GunStore => gunStore;
    public GunAssetsList GunAssetsList { get => gunAssetsList; }

    private IGunStore gunStore;

    private void Awake()
    {
        Instance = this;

        gunStore = new GunStore();
        IItemsLoader itemsLoader = new FakeItemLoader();
        itemsLoader.SendLoadItemsRequest(gunStore);
    }

    private void Start()
    {
        StartCoroutine(WaitForGunStoreInitialisation());
    }

    private IEnumerator WaitForGunStoreInitialisation()
    {
        while (true)
        {
            yield return null;
            if (gunStore.IsInitialized == true)
            {
                break;
            }
        }
        LoadGunsAssets();
    }

    private void LoadGunsAssets()
    {
        List<AssetReference> assetsToLoad = new List<AssetReference>();

        foreach (var gun in gunStore.Guns)
        {
            var gunAsset = gunAssetsList.GetAsset(gun.Id);
            if (gunAsset != null)
            {
                assetsToLoad.Add(gunAsset.Model);
            }
        }

        AddressableHelper.LoadAssetCollection<GameObject>(assetsToLoad, OnAssetsLoaded);
    }

    private void OnAssetsLoaded()
    {
        uiController.OnAllDataLoaded();
    }
}
