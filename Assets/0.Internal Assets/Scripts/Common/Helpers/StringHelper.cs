﻿using System.Globalization;

namespace Common.Helpers
{
    public static class StringHelper
    {
        public static string SeparateThousand(float value, int afterComma = 0)
        {
            string specifier;
            CultureInfo culture;

            specifier = "N" + afterComma.ToString();
            culture = CultureInfo.CreateSpecificCulture("en-US");
            return value.ToString(specifier, culture);
        }

        public static string SeparateThousand(int value)
        {
            string specifier;
            CultureInfo culture;

            specifier = "N0";
            culture = CultureInfo.CreateSpecificCulture("en-US");
            return value.ToString(specifier, culture);
        }
    }
}