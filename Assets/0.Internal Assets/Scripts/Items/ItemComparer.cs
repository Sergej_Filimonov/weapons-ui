﻿using System.Collections.Generic;

namespace Items
{
    public class ItemComparer<TItem> : Comparer<TItem> where TItem : Item
    {
        public override int Compare(TItem x, TItem y)
        {
            return x.CompareTo(y);
        }
    }
}