﻿using UnityEngine;

namespace Guns
{
    public class GunStore : IGunStore
    {
        public bool IsInitialized => isInitialized;
        public GunCatalog Guns => guns;

        private GunCatalog guns;
        private bool isInitialized = false;

        public void OnInitialized(GunCatalog catalog)
        {
            guns = catalog;
            isInitialized = true;
            Debug.Log("GunStor successful initialized!");
        }

        public void OnInitializeFailed()
        {
            Debug.LogError("IGunStore initializing fail!");
        }
    }
}