﻿using Guns;

namespace Networking
{
    public interface IItemsLoader
    {
        void SendLoadItemsRequest(IGunStore store);
    }
}