﻿using Guns;
using System.Collections;
using UnityEngine;
using UnityEngine.AddressableAssets;
using Common.Serialization;

namespace Networking
{
    public class FakeItemLoader : IItemsLoader
    {
        private const string JSON_NAME = "guns-catalog-response";

        private TextAsset textAsset;
        private IGunStore store;

        public void SendLoadItemsRequest(IGunStore store)
        {
            this.store = store;
            ApplicationManager.Instance.StartCoroutine(LoadTextAssetAsync(JSON_NAME));
            Debug.Log("After json start loading...");
        }

        private IEnumerator LoadTextAssetAsync(string assetName)
        {
            Debug.Log("Start loading json...");
            var asyncOperationHandle = Addressables.LoadAssetAsync<TextAsset>(JSON_NAME);
            yield return asyncOperationHandle;
            textAsset = asyncOperationHandle.Result;
            Debug.Log("Json is loaded!");
            ParsText(textAsset.text);
        }

        private void ParsText(string text)
        {
            Debug.Log("Parsing...");
            Response response = (Response)StringSerializationAPI.Deserialize(typeof(Response), text);
            Debug.Log("Parsing successful completed!");

            if (response.status == "success")
            {
                GunCatalog catalog = new GunCatalog();

                foreach (var gunModel in response.response.catalog)
                {
                    GunItem gunItem = new GunItem(gunModel.id, gunModel.name, "", gunModel.level,
                        (Size)gunModel.stats.size, gunModel.stats.range, gunModel.stats.reload_time,
                        new Damage() { Max = gunModel.stats.damage.max, Min = gunModel.stats.damage.min });
                    catalog.Add(gunItem);
                }

                store.OnInitialized(catalog);
            }
            else
            {
                store.OnInitializeFailed();
            }
        }
    }
}