﻿using DG.Tweening;
using UnityEngine;

namespace Common.Views
{
    [RequireComponent(typeof(RectTransform))]
    public class DOTweenShowerAndHider : MonoBehaviour, IShowHandler, IHideHandler
    {
        [SerializeField] private float timeToShow = 0.7f;
        [SerializeField] private float timeToHide = 0.5f;

        private RectTransform rectTransform;
        private Vector3 startShowPosition;
        private Vector3 endHidePosition;

        public void Init(GameObject gameObject)
        {
            gameObject.SetActive(true);
            rectTransform = gameObject.GetComponent<RectTransform>();

            startShowPosition = new Vector3(0, rectTransform.rect.height, 0);
            endHidePosition = new Vector3(0, -rectTransform.rect.height, 0);
            rectTransform.anchoredPosition = startShowPosition;
        }

        public void Show()
        {
            rectTransform.anchoredPosition = startShowPosition;
            rectTransform.DOAnchorPos(Vector3.zero, timeToShow);
        }

        public void Hide()
        {
            rectTransform.DOAnchorPos(endHidePosition, timeToHide);
        }
    }
}