﻿namespace Items
{
    public class ItemNameComparer : ItemComparer<Item>
    {
        public override int Compare(Item x, Item y)
        {
            return string.Compare(x.ItemName, y.ItemName);
        }
    }
}