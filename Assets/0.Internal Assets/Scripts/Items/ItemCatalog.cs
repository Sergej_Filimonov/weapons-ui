﻿using System.Collections;
using System.Collections.Generic;

namespace Items
{
    public class ItemCatalog<TItem> : ICollection<TItem> where TItem : Item
    {
        protected List<TItem> items = new List<TItem>();

        public int Count => items.Count;

        public bool IsReadOnly => false;

        public void Add(TItem item)
        {
            items.Add(item);
        }

        public void Clear()
        {
            items.Clear();
        }

        public bool Contains(TItem item)
        {
            return items.Contains(item);
        }

        public void CopyTo(TItem[] array, int arrayIndex)
        {
            items.CopyTo(array, arrayIndex);
        }

        public IEnumerator<TItem> GetEnumerator()
        {
            return items.GetEnumerator();
        }

        public bool Remove(TItem item)
        {
            return items.Remove(item);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return items.GetEnumerator();
        }

        public int IndexOf(TItem item) => items.IndexOf(item);
    }
}