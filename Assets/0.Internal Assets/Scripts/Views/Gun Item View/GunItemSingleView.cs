﻿using Common.Views;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Guns.Views
{
    public class GunItemSingleView : TargetView<GunItem>
    {
        [SerializeField] private DistanceChanger distanceChanger = null;
        [SerializeField] private Transform modelPosition = null;

        public GunItemAsset GunAsset { get; private set; }

        protected override void InitShowingAndHidingHandlers()
        {
            showHandler = distanceChanger;
            showHandler.Init(modelPosition.gameObject);
            hideHandler = distanceChanger;
        }

        protected override void Init()
        {
            GunAsset = ApplicationManager.Instance.GunAssetsList.GetAsset(Target.Id);

            GameObject obj = Instantiate((GameObject)GunAsset.Model.Asset, modelPosition);
            obj.transform.localPosition = Vector3.zero;
        }
    }
}