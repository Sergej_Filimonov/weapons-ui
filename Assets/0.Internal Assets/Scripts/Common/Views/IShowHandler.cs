﻿using UnityEngine;

namespace Common.Views
{
    public interface IShowHandler
    {
        void Init(GameObject gameObject);
        void Show();
    }
}