﻿namespace Guns
{
    public class GunLevelComparer : GunComparer<GunItem>
    {
        public override int Compare(GunItem x, GunItem y)
        {
            int result = 0;
            if (x.Level < y.Level)
            {
                result = -1;
            }
            else if(x.Level > y.Level)
            {
                result = 1;
            }
            return result;
        }
    }
}