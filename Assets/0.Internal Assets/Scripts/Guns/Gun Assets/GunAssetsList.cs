﻿using System.Linq;
using UnityEngine;

namespace Guns
{
    [CreateAssetMenu(fileName = "Guns Assets List", menuName = "Configs/Guns/Guns Assets List")]
    public class GunAssetsList : ScriptableObject
    {
        [SerializeField] private GunItemAsset[] assets = null;

        public GunItemAsset GetAsset(int id) => assets.FirstOrDefault(g => g.Id == id);
    }
}