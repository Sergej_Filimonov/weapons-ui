﻿using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Guns
{
    [CreateAssetMenu(fileName = "Gun Asset", menuName = "Configs/Guns/Gun Asset")]
    public class GunItemAsset : ScriptableObject
    {
        [SerializeField] private int id = 0;
        [TextArea]
        [SerializeField] private string description = "";
        [SerializeField] private AssetReference model = null;

        public int Id { get => id; }
        public string Description { get => description; }
        public AssetReference Model { get => model; }
    }
}