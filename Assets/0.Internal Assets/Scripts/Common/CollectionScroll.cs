﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

namespace Common
{
    public class CollectionScroll : MonoBehaviour
    {
        [SerializeField] private float scrollSpeed = 60;
        [SerializeField] private Transform objectsContainer = null;
        [Space]
        [SerializeField] private Transform center = null;
        [SerializeField] private Transform left = null;
        [SerializeField] private Transform right = null;
        [SerializeField] private Transform leftOut = null;
        [SerializeField] private Transform rightOut = null;

        public Transform ObjectsContainer { get => objectsContainer; }
        public int Pointer { get; private set; } = 0;
        public int ObjectsCount => transforms.Count;

        private List<Transform> transforms;
        private Transform objectAtCenter;
        private Transform objectAtRight;
        private Transform objectAtLeft;
        private Stack<Transform> leftSide = new Stack<Transform>();
        private Stack<Transform> rightSide = new Stack<Transform>();

        public void SetCollection(List<Transform> collection)
        {
            if (collection == null || collection.Count == 0) return;

            transforms = collection;

            objectAtCenter = transforms[0];
            objectAtCenter.position = center.position;

            if (transforms.Count > 1)
            {
                objectAtRight = transforms[1];
                objectAtRight.position = right.position;
            }

            if (transforms.Count > 2)
            {
                for (int i = transforms.Count - 1; i > 1; i--)
                {
                    transforms[i].position = rightOut.position;
                    rightSide.Push(transforms[i]);
                }
            }
        }

        public void MoveObjects(bool toLeft, bool useAnimation = true)
        {
            if (toLeft)
            {
                if (objectAtRight != null)
                {
                    Pointer++;

                    if (objectAtLeft != null)
                    {
                        if (useAnimation) objectAtLeft.DOMove(leftOut.position, 60 / scrollSpeed);
                        else objectAtLeft.position = leftOut.position;
                        leftSide.Push(objectAtLeft);
                        objectAtLeft = null;
                    }

                    if (useAnimation) objectAtCenter.DOMove(left.position, 60 / scrollSpeed);
                    else objectAtCenter.position = left.position;
                    objectAtLeft = objectAtCenter;
                    objectAtCenter = null;

                    if (useAnimation) objectAtRight.DOMove(center.position, 60 / scrollSpeed);
                    else objectAtRight.position = center.position;
                    objectAtCenter = objectAtRight;
                    objectAtRight = null;

                    if (rightSide.Count > 0)
                    {
                        objectAtRight = rightSide.Pop();
                        if (useAnimation) objectAtRight.DOMove(right.position, 60 / scrollSpeed);
                        else objectAtRight.position = right.position;
                    }
                }
            }
            else
            {
                if (objectAtLeft != null)
                {
                    Pointer--;

                    if (objectAtRight != null)
                    {
                        if (useAnimation) objectAtRight.DOMove(rightOut.position, 60 / scrollSpeed);
                        else objectAtRight.position = rightOut.position;
                        rightSide.Push(objectAtRight);
                        objectAtRight = null;
                    }

                    if (useAnimation) objectAtCenter.DOMove(right.position, 60 / scrollSpeed);
                    else objectAtCenter.position = right.position;
                    objectAtRight = objectAtCenter;
                    objectAtCenter = null;

                    if (useAnimation) objectAtLeft.DOMove(center.position, 60 / scrollSpeed);
                    else objectAtLeft.position = center.position;
                    objectAtCenter = objectAtLeft;
                    objectAtLeft = null;

                    if (leftSide.Count > 0)
                    {
                        objectAtLeft = leftSide.Pop();
                        if (useAnimation) objectAtLeft.DOMove(left.position, 60 / scrollSpeed);
                        else objectAtLeft.position = left.position;
                    }
                }
            }
        }

        public void SetAtCenter(Transform obj)
        {
            if (!transforms.Contains(obj)) return;

            int objIndex = transforms.IndexOf(obj);

            int delta = objIndex - Pointer;

            bool toLeft = delta > 0;

            for (int i = 0; i < Mathf.Abs(delta); i++)
            {
                MoveObjects(toLeft, false);
            }
        }
    }
}