﻿using Common.Views;
using Guns;
using Guns.Views;
using UnityEngine;

namespace UI
{
    public class UIController : MonoBehaviour
    {
        [SerializeField] private GameObject loading = null;
        [SerializeField] private ButtonsPanelView buttonsPanel = null;
        [Space]
        [Header("Gun Store Views")]
        [SerializeField] private GunStoreListView listStoreView = null;
        [SerializeField] private GunStoreGridView gridStoreView = null;
        [SerializeField] private GunStoreSingleView singleStoreView = null;

        public static UIController Insatance;

        private GunStoreView lastView;

        private void Awake()
        {
            Insatance = this;

            loading.SetActive(true);
            buttonsPanel.Hide();

            buttonsPanel.OpenListButtonClicked += OnOpenListButtonClicked;
            buttonsPanel.OpenGridButtonClicked += OnOpenGridButtonClicked;
            buttonsPanel.OpenSingleButtonClicked += OnOpenSingleButtonClicked;
        }

        public void OnAllDataLoaded()
        {
            loading.SetActive(false);
            buttonsPanel.Show();

            singleStoreView.Init(ApplicationManager.Instance.GunStore);
            singleStoreView.HomeButtonClicked += OnStorViewHomeButtonClicked;
            singleStoreView.BackButtonClicked += OnSingleViewBackButtonClicked; ;

            listStoreView.Init(ApplicationManager.Instance.GunStore);
            listStoreView.HomeButtonClicked += OnStorViewHomeButtonClicked;
            listStoreView.BackButtonClicked += OnStorViewHomeButtonClicked;
            listStoreView.Showed += SetLastView;
            listStoreView.ItemViewSelectClicked += giv => OnGunItemSelectButtonClicked(giv, listStoreView);

            gridStoreView.Init(ApplicationManager.Instance.GunStore);
            gridStoreView.HomeButtonClicked += OnStorViewHomeButtonClicked;
            gridStoreView.BackButtonClicked += OnStorViewHomeButtonClicked;
            gridStoreView.Showed += SetLastView;
            gridStoreView.ItemViewSelectClicked += giv => OnGunItemSelectButtonClicked(giv, gridStoreView);
        }

        private void OnSingleViewBackButtonClicked(GunStoreView storeView)
        {
            storeView.Hide();

            if (lastView != null)
            {
                lastView.Show();
            }
            else
            {
                buttonsPanel.Show();
            }
        }

        private void OnOpenGridButtonClicked(ButtonsPanelView buttonsPanel)
        {
            buttonsPanel.Hide();
            gridStoreView.Show();
        }

        private void OnOpenListButtonClicked(ButtonsPanelView buttonsPanel)
        {
            buttonsPanel.Hide();
            listStoreView.Show();
        }

        private void OnOpenSingleButtonClicked(ButtonsPanelView buttonsPanel)
        {
            buttonsPanel.Hide();
            singleStoreView.Show();
        }

        private void OnStorViewHomeButtonClicked(GunStoreView storeView)
        {
            storeView.Hide();
            buttonsPanel.Show();
            lastView = null;
        }

        private void OnGunItemSelectButtonClicked(TargetView<GunItem> view, GunStoreView storeView)
        {
            lastView = storeView;
            lastView.Hide();
            singleStoreView.Show(view.Target);
        }

        private void SetLastView(GunStoreView view)
        {
            lastView = view;
        }
    }
}